#include "dataManage.h"
#include "mainwindow.h"
#include <QDebug>
#include <QObject>

//**note: if this gives a linker error, try running qmake

dataManage::dataManage(QObject *parent) :
    QObject(parent)
{
    connect(this, SIGNAL(propertyChanged(attribInput)), MainWindow::visWidget, SLOT(changeAttribute(attribInput)));
}

void dataManage::updateProperty(int attribInput)
{
    qDebug() << "property updated";
    emit propertyChanged(attribInput);
    //emit whatever you need for your widget, etc.
}

void dataManage::updateHalo(int time, int id)
{
    qDebug() << "halo updated";
}

void dataManage::propertyChanged(int attribInput)
{
    qDebug() << "change emitted";
}
