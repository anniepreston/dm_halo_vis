#include "mainwindow.h"
#include "Visualizer.h"
#include <QApplication>
#include "TFEditor.h"
#include "dataManage.h"

int main(int argc, char *argv[])
{
    QGLFormat glFormat = QGLFormat::defaultFormat();
    glFormat.setVersion(3, 3);
    glFormat.setProfile(QGLFormat::CoreProfile);
    glFormat.setSampleBuffers(true);              //multisample buffer support
    glFormat.setSamples(4);                       //number of samples per pixel
    QGLFormat::setDefaultFormat(glFormat);

    QApplication a(argc, argv);
    Visualizer* visWidget = new Visualizer();
    vv::TFEditor* tfEditor = new vv::TFEditor();
    dataManage* dataManager = new dataManage();

    MainWindow w(visWidget, tfEditor, dataManager);
    w.show();

    return a.exec();
}
