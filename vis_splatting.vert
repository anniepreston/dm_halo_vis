#version 330
layout(location=0)in vec4 Vertex;
layout(location=1)in vec2 Properties;
out vec3 CameraDir;
out vec3 CameraPos;
out float colorProperty; //the attribute that will be colored with transfer function
out float displayProperty; //the feature that will be selected

uniform float aspect;
uniform float cotFOV;
uniform mat4 invModelView;
uniform mat4 transform;
uniform mat4 modelView;
uniform mat3 rotation;
uniform vec3 translation;
uniform mat4 projection;
uniform mat4 scale;

uniform int attribIndex;
uniform int displayAttribIndex;

uniform int dataType;
//uniform sampler3D weightTex;
//uniform sampler3D normalTex;

void main(void)
{
    vec3 dir;
    dir.x = (Vertex.x * 2.0 - 1.0) * aspect;
    dir.y = Vertex.y * 2.0 - 1.0;
    dir.z = -cotFOV;
    CameraDir = mat3(invModelView) * normalize(dir);
    CameraPos = (invModelView * vec4(0.0, 0.0, 0.0, 1.0)).xyz;

    if (attribIndex == 1 || attribIndex == -1){

        if (dataType == 0){
            colorProperty = Vertex.w;
        }
        else {
            //colorProperty = texture(weightTex, vec3(Vertex.rgb)).r;
        }
    }
    else if (attribIndex == 0){
        if (dataType == 0){
            colorProperty = Properties.y;
        }
        else {
            //colorProperty = texture(weightTex, vec3(Vertex.rgb)).g;
        }
    }
    else if (attribIndex == 2){
        if (dataType == 0){
            colorProperty = Properties.x;
        }
        else {
            //colorProperty = texture(weightTex, vec3(Vertex.rgb)).b;
        }
    }

    if (displayAttribIndex == 0){
        displayProperty = Properties.y;
    }
    if (displayAttribIndex == 1){
        displayProperty = Vertex.w;
    }
    if (displayAttribIndex == 2){
        displayProperty = Properties.x;
    }

    gl_Position = transform * invModelView * vec4(Vertex.x, Vertex.y, Vertex.z, 1.0);
}
