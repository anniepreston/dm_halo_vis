#version 330
layout(location=0)in vec4 Vertex;
layout(location=1)in vec2 Properties;
uniform float depth;

out float z_val;
out vec3 data_vals;

void main()
{
    z_val = Vertex.z;
    data_vals = vec3(Vertex.w, Properties.y, Properties.x);

    gl_Position = vec4((2 * vec2(Vertex.x, Vertex.y) - vec2(1.0)), -1.0, 1.0);
}
