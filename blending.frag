#version 330
layout(location=0)out vec4 FragColor0; //weight & weighted "colors"
layout(location=1)out vec4 FragColor1; //normals

uniform float depth;
in vec3 data_vals;
in vec3 position;

void main(void)
{
    float size = 512.;

    vec3 N;
    N.xy = gl_PointCoord*2.0 - vec2(1.0);
    float mag = dot(N.xy, N.xy);

    float weight;
    if (mag < 0.4) {weight = 1 - mag/0.4;}
    else {weight = 0;}

    if (depth > position.z + 1./size || depth < position.z - 1./size) {discard;}

    FragColor0 = vec4(data_vals.x*weight, data_vals.y*weight, data_vals.z*weight, weight);
    FragColor1 = vec4(N.x*weight, N.y*weight, N.z*weight, 0);
}
