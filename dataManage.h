#ifndef DATAMANAGE_H
#define DATAMANAGE_H

#include <QObject>
#include "Visualizer.h"
#include <QWidget>

class dataManage : public QObject
{
    Q_OBJECT

public:
    dataManage(QObject *parent=0);

public slots:
    void updateProperty(int attribInput);
    void updateHalo(int time, int id);

signals:
    void propertyChanged(int attribInput);

private:

};

#endif // DATAMANAGE_H
