#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include "Visualizer.h"
#include "TFEditor.h"
#include "dataManage.h"

class QAction;
class QLabel;
class QMenu;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(Visualizer* visContext, vv::TFEditor* tf, dataManage* dm, QMainWindow *parent = 0);
    vv::TFEditor* tfEditor;
    Visualizer* visWidget;

private:
    dataManage* dataManager;


    QAction *haloAct;
    QAction *particleAct;

    QLabel *ambientLabel;
    QLabel *diffuseLabel;
    QLabel *displayCutoffLabel;
};

#endif // MAINWINDOW_H
