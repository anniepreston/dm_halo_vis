#version 330
//in vec3 CameraDir;
in vec3 CameraPos;
out vec4 FragColor;
in float colorProperty;
in float displayProperty;
uniform float displayCutoff;

uniform sampler2D transferFuncTex;

uniform bool lighting_on;
uniform float ambient;
uniform float diffuse;
uniform int dataType;

vec4 color = texture(transferFuncTex, vec2(colorProperty, 0)).rgba;

vec3 lightDir = vec3(0, 0, 0);
float Ns = 250;
vec4 mat_specular = vec4(1);
vec4 light_specular = vec4(1);

vec3 ambientColor = vec3(0.5, 0.5, 0.5);
vec3 diffuseColor = vec3(0.5, 0.5, 0.5);
vec3 specularColor = vec3(0.1, 0.2, 0.2);

void main(void)
{
    //testing - render points as spheres!
    vec3 N;
    N.xy = gl_PointCoord*2.0 - vec2(1.0);
    float mag = dot(N.xy, N.xy);
    N.z = sqrt(1.0 - mag);

    float diffuse = max(0.0, dot(lightDir, N));

    vec3 eye = CameraPos;
    vec3 halfVector = normalize(eye + lightDir);
    float spec = max(pow(dot(N, halfVector), Ns), 0.);
    vec4 S = light_specular * mat_specular * spec * 0.25;

    if (mag > 0.4) discard;

    if (lighting_on == true){
        color.rgb += vec3(color.r * diffuse * max(0.0, dot(N, lightDir)) * diffuseColor.r,
                      color.g * diffuse * max(0.0, dot(N, lightDir)) * diffuseColor.g,
                      color.b * diffuse * max(0.0, dot(N, lightDir)) * diffuseColor.b);

        color.rgb += vec3(color.r * ambient * ambientColor.r,
                      color.r * ambient * ambientColor.r,
                      color.r * ambient * ambientColor.r);
    }

    if (displayCutoff > 0 && displayProperty < displayCutoff){
        FragColor = vec4(0);
    }
    else {
        FragColor = vec4(color.rgb, 0.2/((mag + 0.5)*(mag + 0.5))) + S;
    }
}
