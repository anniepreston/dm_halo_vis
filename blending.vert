#version 330
layout(location=0)in vec4 Vertex;
layout(location=1)in vec2 Properties;
uniform float depth;

out vec3 data_vals;
out vec3 position;

void main()
{
    data_vals = vec3(Vertex.w, Properties.y, Properties.x);
    position = vec3(Vertex.rgb);

    gl_Position = vec4((2 * vec2(Vertex.x, Vertex.y) - vec2(1.0)), -1.0, 1.0);
}
