#version 330
in vec4 Vertex;

uniform float depth;

out float z_val;
out float data_val;

void main()
{
    z_val = Vertex.z;
    data_val = Vertex.w;

    gl_Position = vec4((2 * vec2(Vertex.x, Vertex.y) - vec2(1.0)), -1.0, 1.0);
}
