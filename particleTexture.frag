#version 330
out vec4 FragColor;
in float z_val;
in vec3 data_vals;
uniform float depth;

void main(void)
{
    float size = 1024.;

    vec3 N;
    N.xy = gl_PointCoord*2.0 - vec2(1.0);
    float mag = dot(N.xy, N.xy);

    if ((z_val > depth - 1./size && z_val < depth + 1./size && mag < 0.7)|| (z_val > depth - 2./size && z_val < depth + 2./size && mag < 0.5)
            || (z_val > depth - 3./size && z_val < depth + 3./size && mag < 0.3))
    {
        FragColor = vec4(data_vals.x, data_vals.y, data_vals.z, data_vals.x);
        //FragColor = vec4(1);
    }
    else discard;
}
