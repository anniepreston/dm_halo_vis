#-------------------------------------------------
#
# Project created by QtCreator 2015-04-07T12:40:59
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets opengl

CONFIG += c++11

TARGET = haloVis
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Visualizer.cpp \
    GLTexture.cpp \
    GLShaderProgram.cpp \
    GLVertexArray.cpp \
    GLArrayBuffer.cpp \
    TF.cpp \
    TFEditor.cpp \
    GLFramebuffer.cpp \
    dataManage.cpp

HEADERS  += mainwindow.h \
    Visualizer.h \
    Mat.h \
    Vec.h \
    MathUtil.h \
    GLTexture.h \
    GLConfig.h \
    GLShaderProgram.h \
    GLVertexArray.h \
    GLArrayBuffer.h \
    octree.h \
    TF.h \
    TFEditor.h \
    GLFramebuffer.h \
    dataManage.h

FORMS    += mainwindow.ui

Data.files += $$OTHER_FILES

macx: Data.path = Contents/MacOS
win32: Data.path = $$OUT_PWD

macx: QMAKE_BUNDLE_DATA += Data
win32: INSTALLS += Data

macx: QMAKE_MAC_SDK = macosx10.9

win32: LIBS += -L$$PWD/glew/ -lglew32
win32: INCLUDEPATH += $$PWD/glew
win32: DEPENDPATH += $$PWD/glew
win32: PRE_TARGETDEPS += $$PWD/glew/glew32.lib
win32: DESTDIR = $$OUT_PWD

OTHER_FILES += \
    vis_splatting.frag \
    vis_splatting.vert \
    tf_cosmo.txt \
    blending.frag \
    blending.vert \
    dataTexture.frag \
    dataTexture.vert \
    raycasting.frag \
    raycasting.vert \
    particleTexture.vert \
    particleTexture.frag
