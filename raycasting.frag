#version 330
in vec3 CameraDir;
in vec3 CameraPos;
out vec4 FragColor;

uniform float ambient;
uniform float diffuse;
//uniform float specular;

uniform vec3 volumeScale;
uniform mat4 invModelView;
uniform sampler3D haloTexture; //need ability to render either of these textures, depending on inputs
uniform sampler3D particleTexture;
uniform sampler2D transferFuncTex;

uniform int dataType;
uniform bool lighting_on;
uniform int attribIndex;
uniform int displayAttribIndex;
uniform int displayCutoff;

//lighting info
vec3 LightPos = vec3 (0, 0, 1);
vec3 ambientColor = vec3(0.1, 0.1, 0.1);
vec3 diffuseColor = vec3(0.1, 0.1, 0.2);
vec3 specularColor = vec3(0.1, 0.2, 0.2);

bool intersectBox(vec3 ori, vec3 dir, vec3 boxMin, vec3 boxMax, out float t0, out float t1)
{
        vec3 invDir = 1.0 / dir;
        vec3 tBot = invDir * (boxMin.xyz - ori);
        vec3 tTop = invDir * (boxMax.xyz - ori);

        vec3 tMin = min(tTop, tBot);
        vec3 tMax = max(tTop, tBot);

        vec2 temp = max(tMin.xx, tMin.yz);
        float tMinMax = max(temp.x, temp.y);
        temp = min(tMax.xx, tMax.yz);
        float tMaxMin = min(temp.x, temp.y);

        bool hit;
        if((tMinMax > tMaxMin))
                hit = false;
        else
                hit = true;

        t0 = tMinMax;
        t1 = tMaxMin;

        return hit;
}

vec3 sampleGrad(sampler3D sampler, vec3 coord)
{
        const int offset = 1;
        float dx = textureOffset(sampler, coord, ivec3(offset, 0, 0)).r - textureOffset(sampler, coord, ivec3(-offset, 0, 0)).r;
        float dy = textureOffset(sampler, coord, ivec3(0, offset, 0)).r - textureOffset(sampler, coord, ivec3(0, -offset, 0)).r;
        float dz = textureOffset(sampler, coord, ivec3(0, 0, offset)).r - textureOffset(sampler, coord, ivec3(0, 0, -offset)).r;
        return vec3(dx, dy, dz);
}

//get texture coordinates
vec3 maptotex(vec3 min, vec3 max, vec3 pos)
{
    return (pos - min)/(max - min);
}

void main(void)
{
    vec3 dir = normalize(CameraDir);
    vec3 pos = CameraPos;

    vec3 boxMin = -volumeScale;
    vec3 boxMax = volumeScale;
    vec3 boxDim = boxMax - boxMin;

    float t0, t1;
    bool hit = intersectBox(pos, dir, boxMin, boxMax, t0, t1);
    if(!hit)
        discard;

    t0 = max(t0, 0.0);
    if(t1 <= t0)
        discard;
    if (t0 < 0.0) t0 = 0.0;

    //ray start/end
    vec3 ray_end = pos + dir * t1;
    pos += dir * t0;

    //float dist = distance(pos, ray_end);

    float minx = min(pos.x, ray_end.x);
    float maxx = max(pos.x, ray_end.x);
    float miny = min(pos.y, ray_end.y);
    float maxy = max(pos.y, ray_end.y);
    float minz = min(pos.z, ray_end.z);
    float maxz = max(pos.z, ray_end.z);

    //color/lighting params
    vec4 color = vec4(0.0);
    vec3 c = vec3(0.0);
    float intensity = 0.0;

    //raycasting
    float step = 0.005;

    while (pos.x >= minx && pos.x <= maxx && pos.y >= miny && pos.y <= maxy
            && pos.z >= minz && pos.z <= maxz && color.a <= 1.0 && color.r < 1.0 &&
            color.g < 1.0 && color.b < 1.0) {

            // normal raycasting:

            vec3 tex = maptotex(boxMin, boxMax, pos);

            float value;
            if (dataType == 0){
                value = texture(haloTexture, tex).r;
            }
            else {
                if (attribIndex == 0){
                    value = texture(particleTexture, tex).b;
                }
                else if (attribIndex == 1 || attribIndex == -1){
                    value = texture(particleTexture, tex).r;
                }
                else if (attribIndex == 2){
                    value = texture(particleTexture, tex).g;
                }
            }
            //limit display to certain values of one attribute
            if (dataType != 0 && displayAttribIndex == 0 && texture(particleTexture, tex).b < displayCutoff){
                value = 0;
            }
            else if (dataType != 0 && displayAttribIndex == 1 && texture(particleTexture, tex).r < displayCutoff){
                value = 0;
            }
            else if (dataType !=0  && displayAttribIndex == 2 && texture(particleTexture, tex).g < displayCutoff){
                value = 0;
            }

            vec4 rgba_sample = texture(transferFuncTex, vec2(value, 0));
            color += rgba_sample;
            //color += vec4(rgba_sample.r, rgba_sample.r, rgba_sample.r, rgba_sample.r);
            //color.a = rgba_sample.r;


            //rgba_sample.a = (1 - pow((1 - rgba_sample.a), (0.005/step))); //general opacity correction (step size)

            int lights = 0;
            //lighting:
            if (lighting_on == true){
                lights = 1;
                vec3 lightdir = normalize(vec3 (LightPos - pos));

                vec3 norm = normalize(sampleGrad(haloTexture, tex));

                vec3 eyedir = normalize(vec3 (CameraPos - pos));

                vec3 refdir = reflect(-lightdir, norm);

                //diffuse:
                c += vec3(rgba_sample.r * diffuse * max(0.0, dot(norm, lightdir)) * diffuseColor.r,
                          rgba_sample.g * diffuse * max(0.0, dot(norm, lightdir)) * diffuseColor.g,
                          rgba_sample.b * diffuse * max(0.0, dot(norm, lightdir)) * diffuseColor.b);

                //ambient
                c += vec3(rgba_sample.r * ambient * ambientColor.r,
                          rgba_sample.g * ambient * ambientColor.g,
                          rgba_sample.b * ambient * ambientColor.b);

                //specular:
                /*
                c += vec3(pow(specular * max(0.0, dot(eyedir, refdir)), 100)*specularColor.r,
                          pow(specular * max(0.0, dot(eyedir, refdir)), 100)*specularColor.g,
                          pow(specular * max(0.0, dot(eyedir, refdir)), 100)*specularColor.b);
                          */
           }
           color += (1 - lights)*(1.0 - color.a) * rgba_sample + lights * vec4(c.r, c.g, c.b, rgba_sample.a) * (1 - color.a);

            pos += dir*step;

        }
    FragColor = color;
}
