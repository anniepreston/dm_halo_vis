#include "Visualizer.h"
#include "mainwindow.h"
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <stdio.h>
#include <iostream>
#include <QIODevice>
#include <QOpenGLFunctions>
#include <QDebug>
#include <QFile>
#include <QString>
#include "octree.h"
#include "dataManage.h"

//share transfer function information
extern int test_res;
extern float *test_colormap;

//share octree stuff
Octree *octree;
OctreeData *octreeData;

Visualizer::Visualizer(const QGLFormat &format, QWidget *parent) :
    QGLWidget(format, parent)
{
    setFocusPolicy(Qt::ClickFocus);
    setMinimumSize(1024, 1024);
}

void Visualizer::initializeGL()
{
    //set attribute & data type
    if (attribute == ""){
        attribute = "velocity";
    }
    if (dataType == "halos"){
        data_int = 0;
    }
    else if (dataType == "particles"){
        data_int = 1;
    }

    //rendering settings
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glEnable(GL_DEPTH_TEST);
    glColorMask(true, true, true, true);
    glDepthMask(true);

    glPolygonMode (GL_FRONT_AND_BACK, GL_FILL); // Polygon rasterization mode (polygon filled)

    //read in halo and particle data from files
    loadHaloData();
    loadParticleData();

    //attrib_blending("particles"); // 'particles' = placeholder for now

    rotation = Mat3f::identity();
    translation = Vec3f(0.0f, 0.0f, -2.0f);
    translation_splatting = Vec3f(0.5f, 0.5f, 0.5f);

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //collect transfer function data
    std::vector<Vec4f> transferFuncData;

    //load transfer function data
    loadTF();

    //resample halos & particles into 3D textures (IF USING RAYCASTING)
    //dataTexture();

    //create vertices for shader programs:
    std::vector<Vec2f> rectVertices;
    rectVertices.push_back(Vec2f(0.0f, 0.0f));
    rectVertices.push_back(Vec2f(0.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 0.0f));
    rectVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    rectVertexBuffer->update(rectVertices.size() * sizeof(Vec2f), &rectVertices.front(), GL_STATIC_DRAW);
    rectVertexArray = GLVertexArray::create();

    //load shaders:
    if (computer == "laptop"){
        visSplattingProgram = GLShaderProgram::create("/Users/anniepreston/dm_halo_vis_repo/dm_halo_vis/vis_splatting.vert", "/Users/anniepreston/dm_halo_vis_repo/dm_halo_vis/vis_splatting.frag");
    }
    else if (computer == "desktop"){
        visSplattingProgram = GLShaderProgram::create("/Users/anniepreston/haloVis/dm_halo_vis/vis_splatting.vert", "/Users/anniepreston/haloVis/dm_halo_vis/vis_splatting.frag");
    }
    if(visSplattingProgram == NULL)
        close();
}

void Visualizer::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
    projectionMatrix = Mat4f::perspective(Math::pi<float>() * 0.25f, float(w) / float(h), 0.01f, 10.0f);
}

void Visualizer::paintGL()
{
    modelViewMatrix = Mat4f(rotation, translation);
    modelViewMatrix_splatting = Mat4f(rotation, translation_splatting);
    Mat4f transform_splatting = Mat4f::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 2.0f, -2.0f);
    Mat4f transform = Mat4f::ortho(0, 1, 0, 1, -1, 1);
    Mat4f invModelView = modelViewMatrix.inverse();
    Mat4f invModelView_splatting = modelViewMatrix_splatting.inverse();

    volumeDim = Vec3i(512);
    Vec3f volumeScale = Vec3f(volumeDim);
    volumeScale = volumeScale / volumeScale.norm();

    //set uniforms for splatting program
    visSplattingProgram->setUniform("invModelView", invModelView_splatting);
    visSplattingProgram->setUniform("transform", transform_splatting);
    visSplattingProgram->setUniform("volumeScale", volumeScale);
    visSplattingProgram->setUniform("modelView", modelViewMatrix_splatting);
    visSplattingProgram->setUniform("rotation", rotation);
    visSplattingProgram->setUniform("translation", translation_splatting);
    visSplattingProgram->setUniform("lighting_on", lighting_on);
    visSplattingProgram->setUniform("ambient", ambient);
    visSplattingProgram->setUniform("diffuse", diffuse);
    visSplattingProgram->setTexture("transferFuncTex", transferFuncTex);
    visSplattingProgram->setUniform("attribIndex", attribIndex);
    visSplattingProgram->setUniform("displayAttribIndex", displayAttribIndex);
    visSplattingProgram->setUniform("displayCutoff", displayCutoff);

    //EXECUTE SHADER PROGRAM
    vis_splatting(dataType);    //splatting
    //raycast(dataType);        //or raycasting
}

void Visualizer::mouseMoveEvent(QMouseEvent *event) //user interaction updates rotation & translation matrices
{
    QPointF pos = event->localPos();
    float dx = pos.x() - mousePos.x();
    float dy = pos.y() - mousePos.y();
    mousePos = pos;

    Qt::MouseButtons mouseButtons = event->buttons();

    if(mouseButtons & Qt::LeftButton)
    {
        rotation = Mat3f::fromAxisAngle(Vec3f::unitY(), dx * 0.005f) * rotation;
        rotation = Mat3f::fromAxisAngle(Vec3f::unitX(), dy * 0.005f) * rotation;
        rotation.orthonormalize();
    }

    if(mouseButtons & Qt::RightButton)
    {
        translation.z += dy * 0.005f;
        translation.z = clamp(translation.z, -9.0f, 1.0f);
    }

    if(mouseButtons & Qt::MidButton)
    {
        float scale = -std::min(translation.z, 0.0f) * 0.001f + 0.000025f;
        translation.x += dx * scale;
        translation.y -= dy * scale;
    }

    updateGL();
}

void Visualizer::mousePressEvent(QMouseEvent *event)
{
    mousePos = event->localPos();
}

void Visualizer::mouseReleaseEvent(QMouseEvent *event)
{
    mousePos = event->localPos();
}

void Visualizer::loadHaloData()
{

    QString halo_files[] = {"SciVisHaloOutput_0", "SciVisHaloOutput_1", "SciVisHaloOutput_2", "SciVisHaloOutput_3", "SciVisHaloOutput_4", "SciVisHaloOutput_5", "SciVisHaloOutput_6", "SciVisHaloOutput_7", "SciVisHaloOutput_8", "SciVisHaloOutput_9",
                            "SciVisHaloOutput_10", "SciVisHaloOutput_11", "SciVisHaloOutput_12", "SciVisHaloOutput_13", "SciVisHaloOutput_14", "SciVisHaloOutput_15", "SciVisHaloOutput_16", "SciVisHaloOutput_17", "SciVisHaloOutput_18", "SciVisHaloOutput_19",
                            "SciVisHaloOutput_20", "SciVisHaloOutput_21", "SciVisHaloOutput_22", "SciVisHaloOutput_23", "SciVisHaloOutput_24", "SciVisHaloOutput_25", "SciVisHaloOutput_26", "SciVisHaloOutput_27", "SciVisHaloOutput_28", "SciVisHaloOutput_29",
                            "SciVisHaloOutput_30", "SciVisHaloOutput_31", "SciVisHaloOutput_32", "SciVisHaloOutput_33", "SciVisHaloOutput_34", "SciVisHaloOutput_35", "SciVisHaloOutput_36", "SciVisHaloOutput_37", "SciVisHaloOutput_38", "SciVisHaloOutput_39",
                            "SciVisHaloOutput_40", "SciVisHaloOutput_41", "SciVisHaloOutput_42", "SciVisHaloOutput_43", "SciVisHaloOutput_44", "SciVisHaloOutput_45", "SciVisHaloOutput_46", "SciVisHaloOutput_47", "SciVisHaloOutput_48", "SciVisHaloOutput_49",
                            "SciVisHaloOutput_50", "SciVisHaloOutput_51", "SciVisHaloOutput_52", "SciVisHaloOutput_53", "SciVisHaloOutput_54", "SciVisHaloOutput_55", "SciVisHaloOutput_56", "SciVisHaloOutput_57", "SciVisHaloOutput_58", "SciVisHaloOutput_59",
                            "SciVisHaloOutput_60", "SciVisHaloOutput_61", "SciVisHaloOutput_62", "SciVisHaloOutput_63", "SciVisHaloOutput_64", "SciVisHaloOutput_65", "SciVisHaloOutput_66", "SciVisHaloOutput_67", "SciVisHaloOutput_68", "SciVisHaloOutput_69",
                            "SciVisHaloOutput_70", "SciVisHaloOutput_71", "SciVisHaloOutput_72", "SciVisHaloOutput_73", "SciVisHaloOutput_74", "SciVisHaloOutput_75", "SciVisHaloOutput_76", "SciVisHaloOutput_77", "SciVisHaloOutput_78", "SciVisHaloOutput_79",
                            "SciVisHaloOutput_80", "SciVisHaloOutput_81", "SciVisHaloOutput_82", "SciVisHaloOutput_83", "SciVisHaloOutput_84", "SciVisHaloOutput_85", "SciVisHaloOutput_86", "SciVisHaloOutput_87", "SciVisHaloOutput_88"};

    QString file_path = "/Users/anniepreston/Desktop/Feature_Extract/";


    int num_halos = 7000; //GET RID OF THIS HARD CODING!
    halo_pos.clear();

    qDebug() << "reading halo data";
    /*
    QString halopath;

    if (computer == "desktop"){
        halopath = "/Users/anniepreston/Desktop/cosmology_datasets/ds14_scivis_0128/rockstar/out_98.list";
    }
    else if (computer == "laptop"){
        halopath = "/Users/anniepreston/out_98.list";
    }
*/
    //QFile halo_file(QString("/Users/anniepreston/Desktop/cosmology_datasets/ds14_scivis_0128/rockstar/out_98.list"));
    QFile halo_file(file_path + halo_files[80]);

    if(!halo_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Can't find or open" << halo_file.fileName();
        return;
    }

    float max_vel = 0.0;
    QTextStream stream(&halo_file);
    QString line;
    for(int i=0; i<1; i++) { //skip header stuff
        line = stream.readLine();
    }
    for(int i=0; i<num_halos; i++) { //read data
        line = stream.readLine();
        QStringList vals = line.split(' ');
        //halo_pos.push_back(Vec4f(vals[8].toFloat()/63., vals[9].toFloat()/63., vals[10].toFloat()/63., vals[2].toFloat()*10e-13)); //position & MASS
        halo_pos.push_back(Vec4f(vals[1].toFloat()/63., vals[2].toFloat()/63., vals[3].toFloat()/63., sqrtf(pow(vals[4].toFloat(), 2) + pow(vals[5].toFloat(), 2) + pow(vals[6].toFloat(), 2))));
        if (vals[1].toFloat() > max_x){
            max_x = vals[1].toFloat();
        }
        if (vals[2].toFloat() > max_y){
            max_y = vals[2].toFloat();
        }
        if (vals[3].toFloat() > max_z){
            max_z = vals[3].toFloat();
        }
        if (vals[3].toFloat() > max_vel){
            max_vel = vals[3].toFloat();
        }
        halo_mass.push_back(vals[2].toFloat());
    }
    halo_file.close();
    qDebug() << "max vel: " << max_vel;

    haloVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    haloVertexBuffer->update(halo_pos.size() * sizeof(Vec4f), &halo_pos.front(), GL_STATIC_DRAW);
    haloVertexArray = GLVertexArray::create();
    haloVertexArray->setAttributeArrayBuffer(haloVertexBuffer, 0, 4, GL_FLOAT, false);
}

void Visualizer::loadTF()
{
    transferFuncData.clear();

    if (tf_file == "")
        tf_file = path + "tf_cosmo.txt";

    if (tf_file == "tf_custom")
    {
        transferFuncData.push_back(Vec4f(0));
        for (int i = 0; i < test_res; i++) //THIS PART MAY NOT BE NECESSARY HERE
        {
            float r = test_colormap[4*i];
            float g = test_colormap[4*i+1];
            float b = test_colormap[4*i+2];
            float a = test_colormap[4*i+3];
            transferFuncData.push_back(Vec4f(r, g, b, a));
        }
    }

    else
    {
        std::ifstream transferfunc(tf_file);
        for(std::string line; std::getline(transferfunc, line); )
        {
            std::istringstream in(line);

            float r, g, b, a;
            in >> r >> g >> b >> a;
            transferFuncData.push_back(Vec4f(r, g, b, a));
            qDebug() << r << g << b << a;
        }

        transferfunc.close();
    }

    transferFuncTex = GLTexture::create(); //put this in header file
    transferFuncTex->updateTexImage2D(GL_RGBA32F, Vec2i(transferFuncData.size(), 1), GL_RGBA, GL_FLOAT, &transferFuncData.front());
}

void Visualizer::loadParticleData()
{
    QString particle_files[] = {"SciVisParticleOutput_0", "SciVisParticleOutput_1", "SciVisParticleOutput_2", "SciVisParticleOutput_3", "SciVisParticleOutput_4", "SciVisParticleOutput_5", "SciVisParticleOutput_6", "SciVisParticleOutput_7", "SciVisParticleOutput_8", "SciVisParticleOutput_9",
                                "SciVisParticleOutput_10", "SciVisParticleOutput_11", "SciVisParticleOutput_12", "SciVisParticleOutput_13", "SciVisParticleOutput_14", "SciVisParticleOutput_15", "SciVisParticleOutput_16", "SciVisParticleOutput_17", "SciVisParticleOutput_18", "SciVisParticleOutput_19",
                                "SciVisParticleOutput_20", "SciVisParticleOutput_21", "SciVisParticleOutput_22", "SciVisParticleOutput_23", "SciVisParticleOutput_24", "SciVisParticleOutput_25", "SciVisParticleOutput_26", "SciVisParticleOutput_27", "SciVisParticleOutput_28", "SciVisParticleOutput_29",
                                "SciVisParticleOutput_30", "SciVisParticleOutput_31", "SciVisParticleOutput_32", "SciVisParticleOutput_33", "SciVisParticleOutput_34", "SciVisParticleOutput_35", "SciVisParticleOutput_36", "SciVisParticleOutput_37", "SciVisParticleOutput_38", "SciVisParticleOutput_39",
                                "SciVisParticleOutput_40", "SciVisParticleOutput_41", "SciVisParticleOutput_42", "SciVisParticleOutput_43", "SciVisParticleOutput_44", "SciVisParticleOutput_45", "SciVisParticleOutput_46", "SciVisParticleOutput_47", "SciVisParticleOutput_48", "SciVisParticleOutput_49",
                                "SciVisParticleOutput_50", "SciVisParticleOutput_51", "SciVisParticleOutput_52", "SciVisParticleOutput_53", "SciVisParticleOutput_54", "SciVisParticleOutput_55", "SciVisParticleOutput_56", "SciVisParticleOutput_57", "SciVisParticleOutput_58", "SciVisParticleOutput_59",
                                "SciVisParticleOutput_60", "SciVisParticleOutput_61", "SciVisParticleOutput_62", "SciVisParticleOutput_63", "SciVisParticleOutput_64", "SciVisParticleOutput_65", "SciVisParticleOutput_66", "SciVisParticleOutput_67", "SciVisParticleOutput_68", "SciVisParticleOutput_69",
                                "SciVisParticleOutput_70", "SciVisParticleOutput_71", "SciVisParticleOutput_72", "SciVisParticleOutput_73", "SciVisParticleOutput_74", "SciVisParticleOutput_75", "SciVisParticleOutput_76", "SciVisParticleOutput_77", "SciVisParticleOutput_78", "SciVisParticleOutput_79",
                                "SciVisParticleOutput_80", "SciVisParticleOutput_81", "SciVisParticleOutput_82", "SciVisParticleOutput_83", "SciVisParticleOutput_84", "SciVisParticleOutput_85", "SciVisParticleOutput_86", "SciVisParticleOutput_87", "SciVisParticleOutput_88", "SciVisParticleOutput_89",
                                "SciVisParticleOutput_90", "SciVisParticleOutput_91", "SciVisParticleOutput_92", "SciVisParticleOutput_93", "SciVisParticleOutput_94", "SciVisParticleOutput_95", "SciVisParticleOutput_96", "SciVisParticleOutput_97", "SciVisParticleOutput_98"};

   particleData.clear();
   QFile particle_file(QString("/Users/anniepreston/Desktop/particleData.txt"));

    if(!particle_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Can't find or open" << particle_file.fileName();
        return;
    }

    particle_count = 0;

    QTextStream stream(&particle_file);
    QString line;
    for(int i=0; i<1; i++) { //skip header stuff
        line = stream.readLine();
    }
    for(int i=0; i<2097152; i++) { //read data
        line = stream.readLine();
        QStringList vals = line.split(' ');
        float vel_mag = sqrtf(vals[4].toFloat()*vals[4].toFloat() + vals[5].toFloat()*vals[5].toFloat() + vals[6].toFloat()*vals[6].toFloat());
        float distance = sqrtf(pow((vals[1].toFloat()/4. - position.x), 2.0) + pow((vals[2].toFloat()/4. - position.y), 2.0) + pow((vals[3].toFloat()/4. - position.z), 2.0));
        if (radius == 0 || (radius > 0 && distance < radius)){
            particleData.push_back(Vec4f(vals[1].toFloat()/8., vals[2].toFloat()/8., vals[3].toFloat()/8., vel_mag/300.));
            particleProperties.push_back(Vec2f(vals[8].toFloat(), vals[7].toFloat()));
            particle_count++;
        }
    }

    particle_file.close();

    partVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    partVertexBuffer->update(particleData.size() * sizeof(Vec4f), &particleData.front(), GL_STATIC_DRAW);
    partPropertiesBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    partPropertiesBuffer->update(particleProperties.size() * sizeof(Vec2f), &particleProperties.front(), GL_STATIC_DRAW);

    partVertexArray = GLVertexArray::create();
    partVertexArray->setAttributeArrayBuffer(partVertexBuffer, 0, 4, GL_FLOAT, false);
    partVertexArray->setAttributeArrayBuffer(partPropertiesBuffer, 1, 2, GL_FLOAT, false);
}

void Visualizer::dataTexture()
{
    textureProgram = GLShaderProgram::create("/Users/anniepreston/haloVis/dm_halo_vis/dataTexture.vert", "/Users/anniepreston/haloVis/dm_halo_vis/dataTexture.frag");
    if(textureProgram == NULL)
        close();

    //render HALOS into texture:
    haloVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    haloVertexBuffer->update(halo_pos.size() * sizeof(Vec4f), &halo_pos.front(), GL_STATIC_DRAW);
    haloVertexArray = GLVertexArray::create();
    haloVertexArray->setAttributeArrayBuffer(haloVertexBuffer, 0, 4, GL_FLOAT, false);

    haloTexture = GLTexture::create(); //texture containing resampled volume data
    haloTexture->updateTexImage3D(GL_R8, Vec3i(512), GL_RED, GL_FLOAT, 0);

    haloFBO = GLFramebuffer::create();
    haloFBO->bind();

    GLenum haloAtt = GL_COLOR_ATTACHMENT0;

    glDrawBuffers(1, &haloAtt);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, 512, 512);
    glDisable(GL_DEPTH_TEST);

    GLuint haloTex = haloTexture->textureID();

    for (int i = 0; i < 512; i++) {

        GLint layer = i;
        float depth = i/float(512);

        textureProgram->begin();

        textureProgram->setUniform("depth", depth);

        haloFBO->attachTexture(haloAtt, haloTexture, 0, layer);

        glBindTexture(GL_TEXTURE_3D, 0);
        glClearColor(0.0, 0.0, 0.0, 0.0); //keep this one black
        glClear(GL_COLOR_BUFFER_BIT);

        glBindTexture(GL_TEXTURE_3D, haloTex);

        glPointSize(10);
        haloVertexArray->drawArrays(GL_POINTS, 7000);
        textureProgram->end();

        glFramebufferTexture3D(GL_FRAMEBUFFER, haloAtt, GL_TEXTURE_3D, 0, 0, layer);
        }

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, 512, 512);
    glDrawBuffers(1, &haloAtt);

    haloFBO->release();

    //render PARTICLES into texture:
    partTextureProgram = GLShaderProgram::create("/Users/anniepreston/haloVis/dm_halo_vis/particleTexture.vert", "/Users/anniepreston/haloVis/dm_halo_vis/particleTexture.frag");
    if(textureProgram == NULL)
        close();

    partVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    partVertexBuffer->update(particleData.size() * sizeof(Vec4f), &particleData.front(), GL_STATIC_DRAW);
    partPropertiesBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    partPropertiesBuffer->update(particleProperties.size() * sizeof(Vec2f), &particleProperties.front(), GL_STATIC_DRAW);

    partVertexArray = GLVertexArray::create();
    partVertexArray->setAttributeArrayBuffer(partVertexBuffer, 0, 4, GL_FLOAT, false);
    partVertexArray->setAttributeArrayBuffer(partPropertiesBuffer, 1, 2, GL_FLOAT, false);

    particleTexture = GLTexture::create(); //texture containing resampled volume data
    particleTexture->updateTexImage3D(GL_RGBA16F, Vec3i(512), GL_RGBA, GL_FLOAT, 0);

    particleFBO = GLFramebuffer::create();
    particleFBO->bind();

    GLenum particleAtt = GL_COLOR_ATTACHMENT0;

    glDrawBuffers(1, &particleAtt);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glViewport(0, 0, 1024, 1024);
    glDisable(GL_DEPTH_TEST);

    GLuint particleTex = particleTexture->textureID();

    for (int i = 0; i < 1024; i++) {

        GLint layer = i;
        float depth = i/float(512);

        partTextureProgram->begin();

        partTextureProgram->setUniform("depth", depth);
        //textureProgram->setUniform("max_vel", max_vel);

        particleFBO->attachTexture(particleAtt, particleTexture, 0, layer);

        glBindTexture(GL_TEXTURE_3D, 0);
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glBindTexture(GL_TEXTURE_3D, particleTex);

        glPointSize(1);
        partVertexArray->drawArrays(GL_POINTS, particle_count);
        partTextureProgram->end();

        glFramebufferTexture3D(GL_FRAMEBUFFER, particleAtt, GL_TEXTURE_3D, 0, 0, layer);
        }

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, width(), height());
    glDrawBuffers(1, &particleAtt);

    particleFBO->release();
}

void Visualizer::vis_splatting(QString dataType)
{
    //render particles or halos using splatting
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPointSize(15);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    if (dataType == "halos"){
        //visSplattingProgram->setVertexAttribute("Vertex", haloVertexArray, haloVertexBuffer, 4, GL_FLOAT, false);
        visSplattingProgram->setUniform("aspect", float(width()) / float(height()));
        visSplattingProgram->setUniform("cotFOV", projectionMatrix.elem[1][1]);

        visSplattingProgram->begin();
        haloVertexArray->drawArrays(GL_POINTS, 7000);
        visSplattingProgram->end();
    }
    else if (dataType == "particles"){
        //visSplattingProgram->setVertexAttribute("Vertex", partVertexArray, partVertexBuffer, 4, GL_FLOAT, false);
        visSplattingProgram->setUniform("aspect", float(width()) / float(height()));
        visSplattingProgram->setUniform("cotFOV", projectionMatrix.elem[1][1]);

        //visSplattingProgram->setTexture("weightTex", weightTex);
        //visSplattingProgram->setTexture("normalTex", normalTex);

        visSplattingProgram->begin();
        partVertexArray->drawArrays(GL_POINTS, particle_count);
        visSplattingProgram->end();
    }
}

void Visualizer::attrib_blending(QString dataType) //attribute blending, for splatting particle data
{
    //create blending shaders
    blendingProgram = GLShaderProgram::create("/Users/anniepreston/haloVis/dm_halo_vis/blending.vert", "/Users/anniepreston/haloVis/dm_halo_vis/blending.frag");
    if (blendingProgram == NULL){
        close();
    }
    blendingProgram->setUniform("dataType", data_int);

    //set up multiple render targets (2D textures) - one for "color" + weighting function, one for normal vector
    normalTex = GLTexture::create();
    normalTex->updateTexImage3D(GL_RGBA16F, Vec3i(512), GL_RGBA, GL_FLOAT, 0);

    weightTex = GLTexture::create();
    weightTex->updateTexImage3D(GL_RGBA16F, Vec3i(512), GL_RGBA, GL_FLOAT, 0);

    blendFBO = GLFramebuffer::create();
    blendFBO->bind();

    GLenum blendAtt[] {GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1};
    glDrawBuffers(2, blendAtt);

    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD); //set blend func/equation so that blending is additive
    glBlendFunc(GL_ONE, GL_ONE);
    glViewport(0, 0, 512, 512);
    glDisable(GL_DEPTH_TEST);

    for (int i = 0; i < 512; i++){
        GLint layer = i;
        float depth = i/512.;

        blendingProgram->begin();

        blendingProgram->setUniform("depth", depth);

        blendFBO->attachTexture(blendAtt[0], weightTex, 0, layer);
        blendFBO->attachTexture(blendAtt[1], normalTex, 0, layer);

        glBindTexture(GL_TEXTURE_3D, 0);
        glClearColor(0.0, 0.0, 0.0, 0.0);
        glClear(GL_COLOR_BUFFER_BIT);

        glPointSize(8);
        partVertexArray->drawArrays(GL_POINTS, particleData.size());
        blendingProgram->end();

        glFramebufferTexture3D(GL_FRAMEBUFFER, blendAtt[0], GL_TEXTURE_3D, 0, 0, layer);
        glFramebufferTexture3D(GL_FRAMEBUFFER, blendAtt[1], GL_TEXTURE_3D, 0, 0, layer);
    }

    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, width(), height());
    glDrawBuffers(2, blendAtt);

    blendFBO->release();
}

void Visualizer::raycast(QString dataType)
{
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // create full screen rectangle for vertex arrays
    std::vector<Vec2f> rectVertices;
    rectVertices.push_back(Vec2f(0.0f, 0.0f));
    rectVertices.push_back(Vec2f(0.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 1.0f));
    rectVertices.push_back(Vec2f(1.0f, 0.0f));
    rectVertexBuffer = GLArrayBuffer::create(GL_ARRAY_BUFFER);
    rectVertexBuffer->update(rectVertices.size() * sizeof(Vec2f), &rectVertices.front(), GL_STATIC_DRAW);
    rectVertexArray = GLVertexArray::create();

    //matrices
    modelViewMatrix = Mat4f(rotation, translation);
    Mat4f transform = Mat4f::ortho(0, 1, 0, 1, -1, 1);
    Mat4f invModelView = modelViewMatrix.inverse();

    //create raycasting program
    raycastingProgram = GLShaderProgram::create("/Users/anniepreston/haloVis/dm_halo_vis/raycasting.vert", "/Users/anniepreston/haloVis/dm_halo_vis/raycasting.frag");
    if(raycastingProgram == NULL)
        close();
    raycastingProgram->setVertexAttribute("Vertex", rectVertexArray, rectVertexBuffer, 2, GL_FLOAT, false);
    //set uniforms
    Vec3f volumeScale = Vec3f(volumeDim);
    volumeScale = volumeScale / volumeScale.norm();
    raycastingProgram->setUniform("volumeScale", volumeScale);
    raycastingProgram->setUniform("transform", transform);
    raycastingProgram->setUniform("invModelView", invModelView);
    raycastingProgram->setUniform("aspect", float(width()) / float(height()));
    raycastingProgram->setTexture("haloTexture", haloTexture);
    raycastingProgram->setTexture("particleTexture", particleTexture);
    raycastingProgram->setTexture("transferFuncTex", transferFuncTex);
    raycastingProgram->setUniform("ambient", ambient);
    raycastingProgram->setUniform("diffuse", diffuse);
    raycastingProgram->setUniform("lighting_on", lighting_on);
    raycastingProgram->setUniform("cotFOV", projectionMatrix.elem[1][1]);
    raycastingProgram->setUniform("dataType", data_int);
    raycastingProgram->setUniform("attribIndex", attribIndex);
    raycastingProgram->setUniform("displayAttribIndex", displayAttribIndex);
    raycastingProgram->setUniform("displayCutoff", displayCutoff);

    raycastingProgram->begin();
    rectVertexArray->drawArrays(GL_TRIANGLE_FAN, 4);
    raycastingProgram->end();
}

void Visualizer::tf_received_test()
{
    tf_file = "tf_custom";
    loadTF();
    updateGL();
}

void Visualizer::changeDataType(QString dataTypeInput)
{
    dataType = dataTypeInput;
    updateGL();
}

void Visualizer::changeAttribute(int attribInput)
{
    std::string attribList[] = {"dot product", "velocity", "variance"};
    attribute = attribList[attribInput];
    attribIndex = attribInput;
    updateGL();
}

void Visualizer::changeDisplayAttribute(int attribInput)
{
    std::string attribList[] = {"dot product", "velocity", "variance"};
    displayAttribute = attribList[attribInput];
    displayAttribIndex = attribInput;
    qDebug() << displayAttribIndex;
    updateGL();
}

void Visualizer::updateLighting(bool checked)
{
    if (checked == true)
    {
        lighting_on = true;
        qDebug() << "lighting on";
    }
    else
    {
        lighting_on = false;
        qDebug() << "lighting off";
    }
    updateGL();
}

void Visualizer::updateAmbient(int ambientVal)
{
    ambient = float(ambientVal / 150.);
    updateGL();
}

void Visualizer::updateDiffuse(int diffuseVal)
{
    diffuse = float(diffuseVal / 150.);
    updateGL();
}

void Visualizer::updateParticles(QListWidgetItem* item)
{
    QString itemtext = item->text();
    QString ind = itemtext.mid(4, 1);
    int index = ind.toInt();

    float density = 2.5e+15;
    position = Vec3f(halo_pos[index].x, halo_pos[index].y, halo_pos[index].z);
    float mvir = halo_mass[index];
    radius = pow((3*mvir)/(4.*3.14*density), 1./3.);

    loadParticleData();
    updateGL();
}

void Visualizer::changeDisplayCutoff(int cutoff)
{
    displayCutoff = cutoff/100.;
    qDebug() << "Cutoff" << displayCutoff;
    updateGL();
}


