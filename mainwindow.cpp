#include <QtGui>

#include "mainwindow.h"
#include "Visualizer.h"
#include "TFEditor.h"
#include <QFile>

MainWindow::MainWindow(Visualizer* visContext, vv::TFEditor* tf, dataManage* dm, QMainWindow *parent) :
    QMainWindow(parent),
    visWidget(visContext),
    tfEditor(tf),
    dataManager(dm)
{    
    setCentralWidget(visWidget);

    setWindowTitle(tr("halos"));

    //attribute display options
    QComboBox *attribBox = new QComboBox();
    attribBox->addItem(tr("velocity"));
    attribBox->addItem(tr("dot product"));
    attribBox->addItem(tr("variance"));

    QComboBox *displayAttribBox = new QComboBox();
    displayAttribBox->addItem(tr("velocity"));
    displayAttribBox->addItem(tr("dot product"));
    displayAttribBox->addItem(tr("variance"));

    QSlider *cutoffSlider = new QSlider(Qt::Horizontal);
    cutoffSlider->setRange(0, 100);
    cutoffSlider->setSingleStep(1);
    cutoffSlider->setValue(50);

    QGroupBox *dataWidget = new QGroupBox(tr(""));
    QToolButton *haloButton = new QToolButton;
    QToolButton *particleButton = new QToolButton;
    QGridLayout *dataLayout = new QGridLayout;
    dataLayout->addWidget(haloButton, 0, 0);
    dataLayout->addWidget(particleButton, 0, 1);
    dataLayout->addWidget(attribBox, 1, 1);
    dataWidget->setLayout(dataLayout);
    dataLayout->addWidget(cutoffSlider, 1, 2);
    dataLayout->addWidget(displayAttribBox, 1, 0);

    QGroupBox *lightingWidget = new QGroupBox(tr(""));

    ambientLabel = new QLabel("ambient");
    QSlider *ambientSlider = new QSlider(Qt::Horizontal);
    ambientSlider->setRange(0, 150);
    ambientSlider->setSingleStep(1);
    ambientSlider->setValue(50);

    diffuseLabel = new QLabel("diffuse");
    QSlider *diffuseSlider = new QSlider(Qt::Horizontal);
    diffuseSlider->setRange(0, 150);
    diffuseSlider->setSingleStep(1);
    diffuseSlider->setValue(50);

    QCheckBox *lightingCheckBox = new QCheckBox("Lighting On");

    QGridLayout *hboxLighting = new QGridLayout;
    hboxLighting->addWidget(ambientLabel, 1, 0);
    hboxLighting->addWidget(ambientSlider, 2, 0);
    hboxLighting->addWidget(diffuseLabel, 3, 0);
    hboxLighting->addWidget(diffuseSlider, 4, 0);

    hboxLighting->addWidget(lightingCheckBox, 9, 0);

    lightingWidget->setLayout(hboxLighting);
    lightingWidget->resize(50, 50);

    QTabWidget *tabWidget = new QTabWidget;
    tabWidget->addTab(tfEditor, tr("TF"));
    tabWidget->setMinimumWidth(500);
    tabWidget->addTab(dataWidget, tr("Data"));
    tabWidget->addTab(lightingWidget, tr("lighting"));

    QDockWidget *stackDock = new QDockWidget(tr(""), this);
    stackDock->setWidget(tabWidget);
    addDockWidget(Qt::LeftDockWidgetArea, stackDock);

    //scrolling halo viewer
    QListWidget *haloList = new QListWidget(this);
    haloList->setViewMode(QListWidget::IconMode);
    haloList->setIconSize(QSize(120,120));
    haloList->setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
    haloList->setWrapping(false);
    haloList->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QFile halo_file(QString("/Users/anniepreston/Desktop/cosmology_datasets/ds14_scivis_0128/rockstar/out_98.list"));
    if(!halo_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        qDebug() << "Can't find or open" << halo_file.fileName();
        return;
    }
    QTextStream stream(&halo_file);
    QString line;
    for(int i=0; i<17; i++) { //skip header stuff
        line = stream.readLine();
    }
    for(int i=0; i<7000; i++) { //read data
        line = stream.readLine();
        QStringList vals = line.split(' ');

        QString num = QString::number(i);
        QString mass = QString::number(vals[2].toFloat());
        QString entry = "halo" + num + ", mass: " + mass;

        QListWidgetItem *halo = new QListWidgetItem(entry, haloList);
        haloList->insertItem(0, halo);
    }
    halo_file.close();

    QDockWidget *haloDock = new QDockWidget(tr(""), this);
    haloDock->setWidget(haloList);
    addDockWidget(Qt::BottomDockWidgetArea, haloDock);

    //data signals/slots/mapper
    haloAct = new QAction(tr("halos"), visWidget);
    particleAct = new QAction(tr("particles"), visWidget);
    QSignalMapper* dataSignalMapper = new QSignalMapper (this);
    connect(haloAct, SIGNAL(triggered()), dataSignalMapper, SLOT(map()));
    connect(particleAct, SIGNAL(triggered()), dataSignalMapper, SLOT(map()));
    QString halos = "halos";
    QString particles = "particles";
    dataSignalMapper -> setMapping (particleAct, particles);
    dataSignalMapper -> setMapping (haloAct, halos);
    connect(dataSignalMapper, SIGNAL(mapped(QString)), visWidget, SLOT(changeDataType(QString)));

    haloButton->setDefaultAction(haloAct);
    particleButton->setDefaultAction(particleAct);

    connect(haloList, SIGNAL(itemClicked(QListWidgetItem*)), visWidget, SLOT(updateParticles(QListWidgetItem*)));

    connect(tfEditor, SIGNAL(changeTFTex()), visWidget, SLOT(tf_received_test()));
    //connect(attribBox, SIGNAL(currentIndexChanged(int)), visWidget, SLOT(changeAttribute(int)));
    connect(attribBox, SIGNAL(currentIndexChanged(int)), dataManager, SLOT(updateProperty(int)));
    connect(displayAttribBox, SIGNAL(currentIndexChanged(int)), visWidget, SLOT(changeDisplayAttribute(int)));

    connect(ambientSlider, SIGNAL(sliderMoved(int)), visWidget, SLOT(updateAmbient(int)));
    connect(diffuseSlider, SIGNAL(sliderMoved(int)), visWidget, SLOT(updateDiffuse(int)));
    connect(lightingCheckBox, SIGNAL(toggled(bool)), visWidget, SLOT(updateLighting(bool)));
    connect(cutoffSlider, SIGNAL(sliderMoved(int)), visWidget, SLOT(changeDisplayCutoff(int)));
}
