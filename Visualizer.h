#ifndef VISUALIZER_H
#define VISUALIZER_H

#include <QGLWidget>
#include <QApplication>
#include "Mat.h"
#include "Vec.h"
#include "GLTexture.h"
#include "GLShaderProgram.h"
#include "GLVertexArray.h"
#include "GLArrayBuffer.h"
#include "GLFramebuffer.h"
#include <QMouseEvent>
#include <string>
#include "octree.h"

class Visualizer : public QGLWidget
{
    Q_OBJECT

public:
    Visualizer(const QGLFormat &format = QGLFormat::defaultFormat(), QWidget *parent = 0);
    virtual void initializeGL();

private:
    void loadHaloData();
    void loadParticleData();
    void loadTF();
    void receiveTF(QString customTFData);
    void preint();
    void vis_splatting(QString dataType);
    void depth_render(QString dataType);
    void attrib_blending(QString dataType);
    void dataTexture();
    void raycast(QString dataType);

protected:
    virtual void resizeGL(int w, int h);
    virtual void paintGL();
    virtual void mouseMoveEvent(QMouseEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    virtual void mouseReleaseEvent(QMouseEvent *event);

    QPointF mousePos;
    Mat3f rotation;
    Vec3f translation;
    Vec3f translation_splatting;
    Mat4f modelViewMatrix;
    Mat4f modelViewMatrix_splatting;
    Mat4f projectionMatrix;
    Vec3i volumeDim;
    float max_x = 0;
    float max_y = 0;
    float max_z = 0;

    std::string path = "/Users/anniepreston/haloVis/dm_halo_vis";

    GLTexture::Ptr transferFuncTex;
    GLTexture::Ptr depthTexture;
    GLShaderProgram::Ptr visSplattingProgram;
    GLShaderProgram::Ptr blendingProgram;
    GLVertexArray::Ptr haloVertexArray;
    GLArrayBuffer::Ptr haloVertexBuffer;
    GLVertexArray::Ptr partVertexArray;
    GLArrayBuffer::Ptr partVertexBuffer;
    GLVertexArray::Ptr rectVertexArray;
    GLArrayBuffer::Ptr rectVertexBuffer;
    GLArrayBuffer::Ptr partPropertiesBuffer;
    GLFramebuffer::Ptr depthFBO;
    GLFramebuffer::Ptr blendFBO;
    GLTexture::Ptr particleTexture;
    GLTexture::Ptr haloTexture;
    GLTexture::Ptr normalTex;
    GLTexture::Ptr weightTex;
    GLShaderProgram::Ptr textureProgram;
    GLFramebuffer::Ptr haloFBO;
    GLFramebuffer::Ptr particleFBO;
    GLShaderProgram::Ptr raycastingProgram;
    GLShaderProgram::Ptr partTextureProgram;

    std::vector<Vec4f> halo_pos;
    std::vector<float> halo_mass;
    std::vector<Vec4f> particleData;
    std::vector<float> particleDot;
    std::vector<float> particleVariance;
    std::vector<Vec2f> particleProperties;
    std::vector<float> halo_dot;
    std::vector<OctreeData*> octreeResults;

    std::string computer = "desktop";
    std::vector<Vec4f> transferFuncData;
    std::string tf_file = "/Users/anniepreston/haloVis/dm_halo_vis/tf_cosmo.txt";

    QString dataType = "halos";
    std::string attribute;
    int attribIndex = -1;
    std::string displayAttribute;
    int displayAttribIndex = -1;
    float displayCutoff;

    bool lighting_on = false;
    float ambient = 0.5;
    float diffuse = 0.5;
    float radius = 0;
    Vec3f position = Vec3f(0);
    int particle_count;

    int data_int;

public slots:
    void tf_received_test();
    void changeDataType(QString dataTypeInput);
    void changeAttribute(int attribInput);
    void changeDisplayAttribute(int attribInput);
    void changeDisplayCutoff(int cutoff);

    void updateAmbient(int ambientVal);
    void updateDiffuse(int diffuseVal);
    void updateLighting(bool);

    void updateParticles(QListWidgetItem* item);
};

#endif // VISUALIZER_H
