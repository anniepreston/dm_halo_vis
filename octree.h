#ifndef OCTREE_H
#define OCTREE_H

#include <vector>

//define OctreeData class

class OctreeData {

    Vec3f position;
    Vec3f velocity;

public:
    OctreeData() {}
    OctreeData(const Vec3f& position, const Vec3f& velocity) : position(position), velocity(velocity) {}
    inline const Vec3f& getPosition() {return position;}
    inline const Vec3f& getVelocity() {return velocity;}
    inline void setPosition(const Vec3f& pos) {position = pos;}
    inline void setVelocity(const Vec3f& vel) {velocity = vel;}
};

//define Octree class

class Octree {

    //inputs = center position, 'radius' of box
    //each node also has: index, depth
    Vec3f center;
    Vec3f radius;

    Octree *children[8]; //tree has 8 children and can store one data point
    OctreeData *data;

public:
    Octree(const Vec3f& center, const Vec3f& radius) : center(center), radius(radius), data(NULL){
        for (int i = 0; i < 8; i++)
        {
            children[i] = NULL;
        }
    }

    Octree(const Octree& copy) : center(copy.center), radius(copy.radius), data(copy.data){

    }

    int getPointOct(const Vec3f& point)
    {
        int oct = 0;
        if (point.x > center.x) oct |= 4;
        if (point.y > center.y) oct |= 2;
        if (point.z > center.z) oct |= 1;
        return oct;
    }

    bool isLeaf() {
        return children[0] == NULL;
    }

    void insert(OctreeData *point) //insert a new data point into the octree
    {
        if(isLeaf())
        {
            if(data==NULL) { //if leaf node (no children) and nothing here, insert the data point
                data = point;
                return;
            }
            else //if leaf node (no children) and something here:
                //1. average new data point with old data point.
                //2. create 8 children and insert new and old data points into the corresponding children
            {
                OctreeData *oldPoint = data;

                Vec3f oldpos = oldPoint->getPosition();
                Vec3f oldvel = oldPoint->getVelocity();
                Vec3f newpos = point->getPosition();
                Vec3f newvel = point->getVelocity();
                Vec3f avgpos = Vec3f((oldpos.x + newpos.x)/2.0, (oldpos.y + newpos.y)/2.0, (oldpos.z + newpos.z)/2.0);
                Vec3f avgvel = Vec3f((oldvel.x + newvel.x)/2.0, (oldvel.y + newvel.y)/2.0, (oldvel.z + newvel.z)/2.0);;
                OctreeData *avgpoint = new OctreeData(avgpos, avgvel);
                data = avgpoint;

                for (int i = 0; i < 8; i++)
                {
                    Vec3f newCenter = center;
                    newCenter.x += radius.x * (i&4 ? 0.5 : -0.5);
                    newCenter.y += radius.y * (i&2 ? 0.5 : -0.5);
                    newCenter.z += radius.z * (i&1 ? 0.5 : -0.5);
                    children[i] = new Octree(newCenter, radius * 0.5f);
                }

                children[getPointOct(oldPoint->getPosition())]->insert(oldPoint);
                children[getPointOct(point->getPosition())]->insert(point);
            }
        }

        else
        {
            //not a leaf (has children):combine as weighted average, depending on # of NON-EMPTY children nodes
            int n = 8;
            for (int i = 0; i < 8; i++){
                if (children[i]->data == NULL)
                {
                    n -= 1;
                }
            }

            OctreeData *oldPoint = data;
            Vec3f oldpos = oldPoint->getPosition();
            Vec3f oldvel = oldPoint->getVelocity();
            Vec3f newpos = point->getPosition();
            Vec3f newvel = point->getVelocity();
            Vec3f avgpos = Vec3f((n*oldpos.x + newpos.x)/float(n+1), (n*oldpos.y + newpos.y)/float(n+1), (n*oldpos.z + newpos.z)/float(n+1));
            Vec3f avgvel = Vec3f((n*oldvel.x + newvel.x)/float(n+1), (n*oldvel.y + newvel.y)/float(n+1), (n*oldvel.z + newvel.z)/float(n+1));
            OctreeData *avgpoint = new OctreeData(avgpos, avgvel);
            data = avgpoint;

            //then, recursively insert into children
            int oct = getPointOct(point->getPosition());
            children[oct]->insert(point);

        }
    }

    //get points inside a bounding box, & GET EACH POINT'S DOT PRODUCT WITH ITS NEAREST NEIGHBOR!

    void getPointsInside(const Vec3f& bmin, const Vec3f& bmax, std::vector<OctreeData*>& results) {
                // If we're at a leaf node, just see if the current data point is inside
                // the query bounding box
                if(isLeaf()) {
                    if(data!=NULL) {
                        const Vec3f& p = data->getPosition();
                        if(p.x>bmax.x || p.y>bmax.y || p.z>bmax.z) return;
                        if(p.x<bmin.x || p.y<bmin.y || p.z<bmin.z) return;
                        results.push_back(data);
                    }
                } else {
                    // We're at an interior node of the tree. We will check to see if
                    // the query bounding box lies outside the octants of this node.
                    for(int i=0; i<8; ++i) {
                        // Compute the min/max corners of this child octant
                        Vec3f cmax = children[i]->center + children[i]->radius;
                        Vec3f cmin = children[i]->center - children[i]->radius;

                        // If the query rectangle is outside the child's bounding box,
                        // then continue
                        if(cmax.x<bmin.x || cmax.y<bmin.y || cmax.z<bmin.z) continue;
                        if(cmin.x>bmax.x || cmin.y>bmax.y || cmin.z>bmax.z) continue;

                        // At this point, we've determined that this child is intersecting
                        // the query bounding box
                        children[i]->getPointsInside(bmin,bmax,results);
                    }
                }
            }
};

#endif // OCTREE_H
